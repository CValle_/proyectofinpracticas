import React, { Component } from 'react';
import {Tab, Tabs} from 'native-base';
import { View } from 'react-native';
import styles from './styles';
import { strings } from 'locales/i18n';

class ReservationList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeReservation: [],
      pageNumber: 1,
      isLoading: false,
      noMoreReservations: false,
      currentTab: this.props.reservations.length > 0 ? 0 : 1,
    };
  };

  renderActiveReservations() {
    return (
      <ReservationCard
      isActiveReservation
      reservationsList={this.state.activeReservation}
      onReservationClicked={item => this.onActiveReservationClicked(item)}
      onTextLinkClicked={item => this.onTextLinkClicked(item)}
      />
    );
  };

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: commonColor.badgeColor }}>
        <CustomText type="h2" _style={styles.headerText}>
        {strings('reservationsList.reservationListTitle')}
        </CustomText>
        <Tabs
        tabBarUnderlineStyle={styles.containerTabStyle}
        initialPage={this.state.currentTab}
        onChangeTab={this.onChangeTab}
        >
          <Tab
          heading={strings('reservationsList.activeHeading')}
          tabStyle={styles.activeTabStayle}
          activeTabStyle={styles.activeTabStyle}
          textStyle={styles.tabTextStyle}
          activeTextStyle={styles.tabTextStyle}
          >
          {this.props.reservations.begin_book
          //...
          }
          </Tab>
        </Tabs>
      </View>
    );
  };
};