USE ECCOCAR;

INSERT INTO USERX(id, email) values(120100, 'randomuser@gmail.com');
INSERT INTO VEHICLE(id, brand, model, license_plate) values(1001, 'Seat', 'Ibiza', '4246FNY');
INSERT INTO BOOKING(id, id_user, id_vehicle, pickup_place, begin_book, end_book, state) values('#P/283/2021-1276', 120100, 1001,'Madrid Airport', '2021-11-15 11:00','2021-11-15 13:00', 'PENDING');



ALTER USER 'root' IDENTIFIED WITH mysql_native_password BY '123456789';
flush privileges;